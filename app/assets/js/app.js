var app = angular.module('app', ['ngRoute', 'foundation']);

// App Configuration
app.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
	$locationProvider.html5Mode(true);

	$routeProvider
	.when('/', {
		templateUrl: myLocalized.templates + 'home.html',
        animation: {
          enter: 'slideInRight',
          leave: 'slideOutRight'
        }
	})
    .when('/test', {
        templateUrl: myLocalized.templates + 'test.html',
        controller: 'testCtrl',
        animation: {
          enter: 'slideInRight',
          leave: 'slideOutRight'
        }
    })
    .otherwise({
        redirectTo: '/'
    });
}]);

app.run(function(){
    FastClick.attach(document.body);
});