app.controller('testCtrl', ['$scope', 'NotificationFactory', function($scope, NotificationFactory){
    $scope.demoTitle = "Demo of Foundation for Apps";
    
    // instantiate the notification set
    var notifSet = new NotificationFactory({
        position: 'bottom-right',
        id: 'main-notifications'
    });
    // on some event, call the following
    notifSet.addNotification({
        title: "Notifications are working!",
        content: "Congratulations!",
        color: 'success'
    });
    
}]);