<!doctype html>
<html lang="en">
  <head>
   <base href="/sites/1tps/">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Starterkit - Digitalz</title>
    <?php wp_head(); ?>
  </head>
  <body ng-app="app">
    <zf-offcanvas id="menu" position="left">
        <a zf-close="" class="close-button">×</a>
    </zf-offcanvas>
    <div class="grid-frame vertical">
      <div class="grid-content shrink" style="padding: 0;">
        <ul class="dark condense menu-bar">
         <li><a zf-open="menu">|||</a></li>
          <li><a href="<?php echo site_url(); ?>"><strong>Starterkit</strong></a></li>
          <li><a href="<?php echo site_url(); ?>/test">Demo</a></li>
        </ul>
      </div>
      <div ng-view class="grid-content">
      </div>
    </div>
  </body>
</html>