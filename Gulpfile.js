//* -----------------
//  GULPFILE.JS
//* -----------------

var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    ngHtml2js = require('gulp-ng-html2js');
    
//* -----------------
//  SASS TASKS
//* -----------------
gulp.task('sass', function() {
  return gulp.src('./app/assets/scss/**/*.scss')
    .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
    }))
    .pipe(sass())
    .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
            cascade: false
        }))
    .pipe(rename({basename: 'style'}))
    .pipe(gulp.dest('./app/assets/css/'))     
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./app/assets/css/'))
});    
    
//* -----------------
//  JAVASCRIPT TASKS
//* -----------------
gulp.task('scripts', function() {
  return gulp.src([
      './app/assets/js/scripts/*.js'  		  
  ])
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./app/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./app/assets/js'))
});    

gulp.task('libs', function() {
  return gulp.src([	
          './libs/fastclick/lib/fastclick.js',
          './libs/viewport-units-buggyfill/viewport-units-buggyfill.js',         
          './libs/tether/tether.js',
          './libs/hammerjs/hammer.js',          
          './libs/foundation-apps/js/vendor/**/*.js',          
          './libs/foundation-apps/js/angular/**/*.js'         
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./app/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./app/assets/js'))
});

//* -----------------
//  FOUNDATION TASKS
//* -----------------

// Compiles the Foundation for Apps directive partials into a single JavaScript file
gulp.task('foundation', function(cb) {
  gulp.src('./libs/foundation-apps/js/angular/components/**/*.html')
    .pipe(ngHtml2js({
      prefix: 'components/',
      moduleName: 'foundation',
      declareModule: false
    }))
    .pipe(uglify())
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('./app/assets/js'))
  ;

  // Iconic SVG icons
  gulp.src('./libs/foundation-apps/iconic/**/*')
    .pipe(gulp.dest('./app/assets/img/iconic/'))
  ;

  cb();
});

//* -----------------
//  DEFAULT TASK
//* -----------------

gulp.task('default', function() {
  gulp.start('sass', 'scripts', 'libs', 'foundation');
});

//* -----------------
//  WATCH TASK
//* -----------------

gulp.task('watch', function() {

  gulp.watch('./app/assets/scss/**/*.scss', ['sass']);
  gulp.watch('./app/assets/js/scripts/*.js', ['scripts']);
  gulp.watch('./libs/foundation-apps/js/*.js', ['foundation']);

});