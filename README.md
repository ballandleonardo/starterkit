# README #

Welcome to the new Starterkit by Digitalz. It is a Wordpress Theme using Foundation for Apps by Zurb to explore new ways of design, also AngularJS and the Wordpress JSON API are solid foundations to build websites and web-apps easier and faster.

### Informations ###

* DIGITALZ is a web digital company, working with clients from Brasil and France.
* Starterkit Version 2.0.0
* [StarterKit Homepage](https://bitbucket.org/leodsidz/starterkit)

### How do I get set up? ###

* Download or Clone the repo

```
#!cmd

clone https://leodsidz@bitbucket.org/leodsidz/starterkit.git
```

* Install the dev dependencies
```
#!cmd

npm install
```

* Initiate the assets files (scss compiling, js concat, minifying everything and more...)

```
#!cmd

gulp
```
* Live Rebuilding of the assets
```
#!cmd

gulp watch
```


### Contributions ###

* Big thanks to ZURB to develop this awesome framework
* The AngularJS Team for developping an amazing product
* The Wordpress Team and the idea of API to change the way we are imagining the web
* Starterkit inspired by JointsWP
* Don't hesitate to contact me if you got any questions.

### Who do I talk to? ###

* Leonardo da Silva - BR/FR
* digitalz4k@gmail.com