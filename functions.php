<?php

function load_js() {

	wp_deregister_script('jquery');
    
    wp_enqueue_script(
		'angularjs',
		get_stylesheet_directory_uri() . '/libs/angular/angular.min.js'
	);
    
    wp_enqueue_script(
		'angularjs-route',
		get_stylesheet_directory_uri() . '/libs/angular-route/angular-route.min.js'
	);
    
    wp_enqueue_script(
        'libs-js',
        get_template_directory_uri() . '/app/assets/js/libs.min.js'
    ); 
    
    wp_enqueue_script(
		'load_angular',
		get_stylesheet_directory_uri() . '/app/assets/js/app.js',
        array( 'angularjs-route', 'angularjs' )
	);
    
    wp_enqueue_script(
        'test-controller',
        get_template_directory_uri() . '/app/assets/js/Controllers/testCtrl.js'
    ); 
    
    wp_enqueue_script(
        'templates-js',
        get_template_directory_uri() . '/app/assets/js/templates.js'
    );
    
    wp_enqueue_style(
        'site-css',
        get_stylesheet_directory_uri() . '/app/assets/css/style.min.css',
        array(), '', 'all'
    );
    wp_localize_script(
		'load_angular',
		'myLocalized',
		array(
			'templates' => trailingslashit( get_template_directory_uri() ) . 'app/templates/'
			)
	);

}
add_action( 'wp_enqueue_scripts', 'load_js' );

?>